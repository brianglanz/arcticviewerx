## ParaView ArcticViewer - The ultimate data viewer!

[![License](https://img.shields.io/badge/License-BSD-lightgray.svg?style=flat-square)](https://gitlab.com/brianglanz/arcticviewerx/-/blob/master/LICENSE)

### Goal ###

Browser exploration of sensitive data generated InSitu or in batch, capable of Type 1 certification ([CNSSI No. 4009 (PDF)](https://rmf.org/wp-content/uploads/2017/10/CNSSI-4009.pdf)).
## Installation

```
$ npm install -g arctic-viewer
```

After installing the package, you will get an executable **ArcticViewer** with
these options:

```
$ ArcticViewer

  Usage: ArcticViewer [options]

  Options:

    -h, --help                            Output usage information
    -V, --version                         Output the version number
    -p, --port [3000]                     Start web server with given port
    -d, --data [directory/http]           Data directory to serve
    -s, --server-only                     Do not open the web browser

    -o, --output-pattern [path/pattern]   Provide a path/pattern for the exported images

    --download-sample-data                Choose data to download inside current directory
    --download [http://remote-host/data]  Download remote data inside current directory

    -M, --magic-lens                      Enable MagicLens inside client configuration
    -S, --single-view                     Enable SingleView inside client configuration
    -R, --recording                       Enable Recording inside client configuration
    -D, --development                     Enable Development inside client configuration

```

Try it out by downloading sample data
(unless you already have some :) and running the viewer.

An example of how to download sample data:

```sh
$ mkdir sample-data && cd $_
$ ArcticViewer --download-sample-data

 | Available datasets for download (path: /tmp)
 |   (1)  40.0 MB  -  diskout-composite
 |   (2)  94.2 MB  -  ensemble
 |   (3)   292 KB  -  garfield
 |   (4)  13.7 MB  -  head_ct_3_features
 |   (5)  13.1 MB  -  head_ct_4_features
 |   (6)  50.8 MB  -  hydra-image-fluid-velocity
 |   (7) 162.3 MB  -  mpas-composite-earth
 |   (8)  37.5 MB  -  mpas-flat-earth-prober
 |   (9) 552.5 MB  -  mpas-hd-500-7t
 |
 | Press Enter to quit or the dataset number to download: 1
 | Press Enter to quit or the dataset number to download: 5
 | Press Enter to quit or the dataset number to download: 8
 | Press Enter to quit or the dataset number to download:
 |
 | => You will be able to try ArcticViewer with these commands:
 |
 |  $ ArcticViewer -d /tmp/head_ct_4_features
 |  $ ArcticViewer -d /tmp/diskout-composite
 |  $ ArcticViewer -d /tmp/mpas-flat-earth-prober
 |
 | Thank you for trying this out...

```

Then you can view the sample data with the provided feedback, or by running:

```sh
$ ArcticViewer -d ./sample-data/mpas-probe-flat-earth
```

This will load [MPAS oceanic simulation data](https://mpas-dev.github.io/) that represent a 3D volume of a
flattened version of the Earth with temperature and salinity information on the oceans.

From those data you can look at a slice along any axis, and move the
slice back and forth using the scroll of your input device.

If you want to zoom or pan, scroll+[any modifier key] or drag+[any modifier key].

```sh
$ ArcticViewer -d ./sample-data/hydra-image-fluid-velocity
```

This will load [Hydra CFD](https://www.mpls.ox.ac.uk/research-section/the-hydra-code-rolls-royces-standard-aerodynamic-design-tool) simulation data that represent the fluid velocity
using volume rendering techniques.

## Documentation

See Kitware's [documentation](https://kitware.github.io/arctic-viewer) for a
getting started guide, advanced documentation, mobile client, API descriptions, and more.

#### Licensing

**arctic-viewer** aka **ArcticViewer** is licensed under [BSD Clause 3](LICENSE).

#### Getting Involved

Fork [Kitware's repository](https://github.com/Kitware/arctic-viewer) and do great things. They've been contributing to open-source software for 15 years and counting, and want to make **arctic-viewer** useful to as many people as possible.
